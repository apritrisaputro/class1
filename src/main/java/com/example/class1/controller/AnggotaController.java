package com.example.class1.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.class1.model.AnggotaModel;
import com.example.class1.model.FakultasModel;
import com.example.class1.service.AnggotaService;
import com.example.class1.service.FakultasService;

import java.util.ArrayList;
import java.util.List;

@Controller // menyambungkan/memanggil data untuk dijadikan link dan menjalankan perintah2
@RequestMapping("/anggota") // meminta data dari folder fakultas
public class AnggotaController {

	@Autowired
	private AnggotaService anggotaService; // memanggil FakultasService (asli) dan menamai dengan fakultasService

	@Autowired
	private FakultasService fakultasService;

	@RequestMapping("/home")
	public String anggota() {
		return "/anggota/home";
	}

	@RequestMapping("/add")
	public String doAdd(Model model) {
		this.doListFakultas(model);
		return "/anggota/add";
	}

	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) { // request untuk mengambil data yang kita akan masukan dan
														// membuatnya di database
		Integer idAnggota = Integer.parseInt(request.getParameter("idAnggota"));
		String kodeAnggota = request.getParameter("kodeAnggota");
		String namaAnggota = request.getParameter("namaAnggota");
		Integer usia = Integer.parseInt(request.getParameter("usia"));
		String kodeFakultas = request.getParameter("kodeFakultas");

		AnggotaModel anggotaModel = new AnggotaModel(); // instance
		anggotaModel.setIdAnggota(idAnggota);
		anggotaModel.setKodeAnggota(kodeAnggota);
		anggotaModel.setNamaAnggota(namaAnggota);
		anggotaModel.setUsia(usia);
		anggotaModel.setKodeFakultas(kodeFakultas);

		this.anggotaService.save(anggotaModel);

		return "/anggota/home";
	}

	@RequestMapping("/data")
	public String doList(Model model) // untuk mengirim data dari back ke front end
	// model dibuat untuk melempar data
	{
		List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
		anggotaModelList = this.anggotaService.read(); // read untuk membaca data fakultasService
		model.addAttribute("anggotaModelList", anggotaModelList); // ("") dibundle dan dijadikan nama fakultasModelList

		return "/anggota/list"; // menjalankan folder fakultas lalu ke data list.
	}

	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model) {// untuk mengirim data dari back ke front end
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
		model.addAttribute("anggotaModel", anggotaModel);

		return "/anggota/detail"; // menjalankan folder fakultas lalu ke data list.
	}

	@RequestMapping("ubah") // untuk pop up
	public String doUbah(HttpServletRequest request, Model model) {
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
		model.addAttribute("anggotaModel", anggotaModel);

		return "/anggota/edit";
	}

	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request) { // request untuk mengambil data yang kita akan masukan dan
															// membuatnya di database
		Integer idAnggota = Integer.parseInt(request.getParameter("idAnggota"));
		String kodeAnggota = request.getParameter("kodeAnggota");
		String namaAnggota = request.getParameter("namaAnggota");
		Integer usia = Integer.parseInt(request.getParameter("usia"));
		String kodeFakultas = request.getParameter("kodeFakultas");

		AnggotaModel anggotaModel = new AnggotaModel(); // instance
		anggotaModel.setIdAnggota(idAnggota);
		anggotaModel.setKodeAnggota(kodeAnggota);
		anggotaModel.setNamaAnggota(namaAnggota);
		anggotaModel.setUsia(usia);
		anggotaModel.setKodeFakultas(kodeFakultas);

		this.anggotaService.update(anggotaModel);
		return "/anggota/home";
	}

	public void doListFakultas(Model model) {
		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
		fakultasModelList = this.fakultasService.read();
		model.addAttribute("fakultasModelList", fakultasModelList);
	}

	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model) {
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);

		this.anggotaService.delete(anggotaModel);

		return "/anggota/home";
	}

	@RequestMapping("/search")
	public String doSearchNama(HttpServletRequest request, Model model) {
		String namaAnggota = request.getParameter("namaAnggota"); // text field dari fakultas

		List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>(); // untuk membuat wadah
		anggotaModelList = this.anggotaService.searchNamaAnggota(namaAnggota); // yang akan di ikat
		model.addAttribute("anggotaModelList", anggotaModelList); // berfungsi untuk membungkus dan yang akan dikirimkan

		return "/anggota/search";
	}
}